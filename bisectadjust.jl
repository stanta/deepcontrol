#bisect and adjust v.04
using Random
#, Plots


function bisect_adjust(markfrom, mark1, mark2, threshold)
    println(markfrom, mark1, mark2)
    (n,d) = size(X)
    if markfrom == 0
        clab_map = rand([mark1, mark2], n, d)  # intial random bisest   
    else
        for i in 1:n
            for j in 1:d
                if clab_map[i,j] == markfrom 
                    clab_map[i,j] = rand([mark1, mark2])
                end    
            end
        end
    end
 
    disttoD0 = disttoD1 = Array{Float64,2}(undef,n,d) 
    class0_D = class1_D = 0.0
    class0_count = class1_count = 0

    totaldists = 1     

    while true #sad but...
    # find D
        for i in 1:n
            for j in 1:d
                if clab_map[i,j] == mark1
                    class0_D += X[i,j]
                    class0_count += 1
                elseif clab_map[i,j] == mark2
                    class1_D += X[i,j]
                    class1_count += 1
                end
            end
        end
        D0 = class0_D / class0_count
        D1 = class1_D / class1_count

        # distances to D
        clab_map0_count = clab_map1_count = 0
        maxdists0 =  maxdists1 = Array{Tuple{Int64, Int64},1}
        # TupleFloat64,Float64},1}
        totaldistD0 = totaldistD1 = maxdist0 = maxdist1 = 0

        for i in 1:n
            for j in 1:d
                disttoD0[i,j] = abs(D0 - X[i,j])
                disttoD1[i,j] = abs(D1 - X[i,j])
                if disttoD0[i,j] < disttoD1[i,j]
                    clab_map[i,j] = mark1
                    clab_map0_count += 1 
                    totaldistD0 += disttoD0[i,j]
                    if disttoD0[i,j]  >  maxdist0
                        maxdist0 = disttoD0[i,j]
                        push!(maxdists0, tuple(i,j))    
                    end 
                else
                    clab_map[i,j] = mark2
                    clab_map1_count += 1 
                    totaldistD1 += disttoD1[i,j]
                    if disttoD1[i,j]  > maxdist1 
                        maxdist1 = disttoD1[i,j]
                        push!(maxdists1, tuple(i,j))    
                    end 
                end    
            end    
        end


        #adjusting
        if clab_map0_count > clab_map1_count
            for m in 1:(clab_map0_count - clab_map1_count) ÷ 2            
                # find for max element
               (i,j) = maxdists0.pop!
                # drops to another cluster
                clab_map[i,j] = mark2
               
            end
        else
            for m in 1:(clab_map1_count - clab_map0_count) ÷ 2            
                # find for max element
               (i,j) = maxdists0.pop!
                # drops to another cluster
            
                clab_map[i,j] = mark1
            
            end
        end
        # convergence
        
        
         if((totaldistD0 + totaldistD1) / totaldists - 1) < threshold
            return clab_map 
        else 
            totaldists = totaldistD0 + totaldistD1
        end

    end    
end


function two_means(k::Int64,t::Int64, markfrom::Int64, mark1::Int64, mark2::Int64,  threshold::Float64) 
    # ::Array{Int64,2}, ::Int64, ::Int64, ::Int64, ::Int64, ::Int64, ::Float64

    clab_map = bisect_adjust( markfrom, mark1, mark2, threshold)
    if t < k 
        t += 1
        mark11 = mark1*10*t + 0
        mark12 = mark1*10*t + 1
        mark21 = mark2*10*t + 2
        mark22 = mark2*10*t + 3
        ( mark11, mark12, mark21, mark22) = two_means( k,t, mark1, mark11, mark12,  threshold)
        ( mark11, mark12, mark21, mark22) = two_means( k,t, mark2, mark21, mark22,  threshold)
    end    
    return (clab_map,  mark11, mark12, mark21, mark22)
end

function Smap(k)
(n,d) = size(X)
    for i in 1:n
        for j in 1:d
            for t in 1:k
                if clab_map[i,j] == mark1
                    S0[i,j] = X[i,j]
                elseif clab_map[i,j] == mark2
                    S1[i,j] = X[i,j]
                end
            end
        end
    end
return S_map
end

n = 100
d = 100
X = rand(0:1, n, d)
k = 8
t = 1
threshold = 0.01
markfrom0 = 0
mark1 = 1
mark2 = 2


clab_map = Array{Int64,2}(undef,n,d) #  rand([mark1, mark2], n, d)  # intial random bisest   

(mark11, mark12, mark21, mark22) = two_means(k, t, markfrom0, mark1, mark2, threshold ) # get map of clusters






#scatter(n, d, marker_z=clab_map,  color=:lightrainbow, legend=false)