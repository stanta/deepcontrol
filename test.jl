
using Random
# Algorithm 1. TwoMeans(Xn×d, k)
#1: Input: matrix Xn×d
n = 100;
d = 1000;
X = Matrix{Float64}(undef, n, d)
randn(X, Float64, (n,d))
k = 8 # number of k- means
#2: Output: matrix cLabeln×1
S = X
t = 1
# https://github.com/stanta/Clustering.jl/

struct KmeansResult{C<:AbstractMatrix{<:AbstractFloat},D<:Real,WC<:Real} <: ClusteringResult
    centers::C                 # cluster centers (d x k)
    assignments::Vector{Int}   # assignments (n)
    costs::Vector{D}           # cost of the assignments (n)
    counts::Vector{Int}        # number of points assigned to each cluster (k)
    wcounts::Vector{WC}        # cluster weights (k)
    totalcost::D               # total cost (i.e. objective)
    iterations::Int            # number of elapsed iterations
    converged::Bool            # whether the procedure converged
end

#4: cLabel[1· · ·n] ← 1;
#cLabel = Vector{Float64}(1, n)

#5: Map cLabel[1· · ·n] to partition S;

#6: while t < k do
#7: Pop Si with largest size out of S


#8: Bisect Si into Su and Sv
function boost_2kmeans(X) 
#=
https://arxiv.org/pdf/1610.02483,.pdf

    1: Input: matrix Xn×d
2: Output: S1, · · ·, Sr, · · ·Sk
3: Assign xi ∈ X with a random cluster label;
=#

#=
4: Calculate D1, · · ·, Dr, · · ·Dk and I
Dr = Sum(xi) xi

5: while not convergence do
6: for each xi ∈ X (in random order) do
7: Seek Sv that maximizes ∆I
∗
1
(xi);
8: if ∆I
∗
1
(xi) > 0 then
9: Move xi from current cluster to Sv;
10: end if
11: end for
12: end while
=#
    return (Su, Sv)
    
end

function bisect (X)
    
    #=
    Algorithm 2. Bisecting Boost k-means
1: Input: matrix Xn×d
2: Output: S1, · · ·, Sr, · · ·Sk
3: Treat X as one cluster S1;
4: Push S1 into a priority queue Q;
5: i = 1;
6: while i < k do
7: Pop cluster Si from queue Q
8: Call Alg. 1 to bisect Si
into {Si
, Si+1};
9: Push Si
, Si+1 into queue Q;
10: i = i + 1;
11: end while
    =#
    return Su, Sv
    
end     
#9: Adjust Su and Sv to equal size

function  adjust (Su, Sv)
    #
    return  (adj_Su, adj_Sv)
    
end

function two_means(X, S, k, t, clabels)
    
    Su, Sv = adjust (bisect (S))
    clabels[k].push(Su, Sv)
    if t < k 
        t++
        two_means(X, Su, k, t, clabels)    
        two_means(X, Sv, k, t, clabels)    
    end    

    return
end


#10: S ← S ∪ Su ∪ Sv
#11: t = t + 1;
#12: end while
# 13: Map S to cLabel[1· · ·n];