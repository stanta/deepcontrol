using Clustering

function tomeans (_x)
    # make a random dataset with 1000 random 5-dimensional points


    # cluster X into 20 clusters using K-means
    R = kmeans(_x, 2; maxiter=200, display=:iter)

    @assert nclusters(R) == 2 # verify the number of clusters
    return R

end


X = rand(5, 1000)

tomeans (X)
a = assignments(R) # get the assignments of points to clusters
c = counts(R) # get the cluster sizes
M = R.centers # get the cluster centers
