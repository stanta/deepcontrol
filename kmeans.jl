function Classify{T<:Any}(distances::Array{Float64,1}, labels::Array{T,1}, k::Int)
    class = unique(labels) 
    nc = length(class) #number of classes
    indexes = Array(Int,k) #initialize vector of indexes of the nearest neighbors
    M = typemax(typeof(distances[1])) #the largest possible number that this vector can have
    class_count = zeros(Int, nc) 
    for i in 1:k
        indexes[i] = indmin(distances) #returns index of the minimum element in a collection
        distances[indexes[i]] = M #make sure this element is not selected again
    end
    klabels = labels[indexes]
    for i in 1:nc
        for j in 1:k
            if klabels[j] == class[i]
                class_count[i] +=1
            end
        end
    end
    m, index = findmax(class_count)
    conf = m/k #confidence of prediction
    return class[index], conf
end